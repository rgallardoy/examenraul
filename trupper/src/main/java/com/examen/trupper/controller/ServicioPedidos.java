package com.examen.trupper.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.trupper.entity.Pedidos;
import com.examen.trupper.repository.PedidosRepository;

@RestController
@RequestMapping("/truper")
public class ServicioPedidos {
	
	@Autowired
	private PedidosRepository pedidosRepository;
	
	@GetMapping("/pedidos")
	public  List<Pedidos>  getPedidos(){
		
		return pedidosRepository.findAll();
		
	}
	
	@PostMapping("/agregarpedido")
	public Pedidos cearPedido(@RequestBody Pedidos pedido) {
		
		return pedidosRepository.save(pedido);
	}
	
	
	

}
