package com.examen.trupper.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PEDIDOS_W")
@Data @AllArgsConstructor @NoArgsConstructor
public class Pedidos implements Serializable {


private static final long serialVersionUID = 751278107182878454L;



@Column(name = "ID_PEDIDO")
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int idPedido;
@Column(name = "TOTAL")
private double total;
@Column(name = "DATE_SALE ")
@Temporal(TemporalType.DATE)
private Date dateSale;


/*
 
 CREATE TABLE examen.PEDIDOS_W(
ID INTEGER PRIMARY KEY AUTO_INCREMENT,
TOTAL DOUBLE,
,DATE_SALE DATE
)ENGINE=INNODB;

CREATE TABLE examen.PEDIDOS_DETALLE_W(
ID INTEGER PRIMARY KEY AUTO_INCREMENT,
ID_PEDIDO INTEGER,
SKU VARCHAR(150),
AMOUT DOUBLE,
PRICE DOUBLE,
FOREIGN KEY (ID_PEDIDO) REFERENCES PEDIDOS_W(ID),
)ENGINE=INNODB;
 
 
 * 
 * */

}
