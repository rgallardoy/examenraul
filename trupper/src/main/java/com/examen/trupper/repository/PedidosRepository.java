package com.examen.trupper.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;

import com.examen.trupper.entity.Pedidos;


public interface PedidosRepository extends JpaRepository<Pedidos, Long> {
	
	public List<Pedidos> findAll();

}
